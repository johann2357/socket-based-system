import sys
import socket
import select
# import os
import subprocess
from server import Chunks


class Client(object):
    data_to_sent = ""
    chunks = []

    def run(self):
        chu = Chunks()
        if(len(sys.argv) < 3):
            print 'Usage : python chat_client.py hostname port'
            sys.exit()

        host = sys.argv[1]
        port = int(sys.argv[2])

        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.settimeout(2)

        # connect to remote host
        try:
            s.connect((host, port))
        except:
            print 'Unable to connect'
            sys.exit()

        print 'Connected to remote host. You can start sending messages'
        sys.stdout.write('[Me] ')
        sys.stdout.flush()

        while 1:
            socket_list = [sys.stdin, s]

            # Get the list sockets which are readable
            read_sockets, write_sockets, error_sockets = select.select(
                socket_list,
                [],
                []
            )

            for sock in read_sockets:
                if sock == s:
                    # incoming message from remote server, s
                    data = sock.recv(4096)
                    if not data:
                        print '\nDisconnected from chat server'
                        sys.exit()
                    else:
                        # print data comment
                        # sys.stdout.write(data)
                        self.chunks.append(data)
                        chu.save_chunks("exec.out", self.chunks)
                        # sys.stdout.write('[Me] ')
                        sys.stdout.flush()

                else:
                    # user entered a message
                    # msg = sys.stdin.readline()
                    self.exec_file("test.txt")
                    s.send(self.data_to_sent)
                    # sys.stdout.write('[Me] ')
                    sys.stdout.flush()

    def get_file(self, data):
        # return file_route
        # function johan
        pass

    def sent_to_server(self, file_route):
        # aqui funcion de johan
        return self.data_to_sent

    def exec_file(self, file_route):
        # os.system("./*.out")
        data_printed = subprocess.check_output("./*.out", shell=True)
        print "data_printed: ", data_printed
        text_file = open(file_route+"_client.txt", "w")
        text_file.write(data_printed)
        text_file.close()
        self.sent_to_server(file_route)  # data is in variable data_to_sent


if __name__ == "__main__":
    client = Client()
    # client.exec_file("test.out")
    sys.exit(client.run())
