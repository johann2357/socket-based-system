import os
import sys
import socket
import select


class Client(object):

    def __init__(self, host, port):
        self.HOST = host
        self.PORT = port

    def read_into_buffer(self, filename):
        buf = bytearray(os.path.getsize(filename))
        with open(filename, 'rb') as f:
            f.readinto(buf)
        return buf

    def chunk_generator(self, file_name, chunk_size):
        buf = self.read_into_buffer(file_name)
        size = os.path.getsize(file_name)
        begin, end = 0, chunk_size
        i = 0
        while end < size:
            tmp = bytearray(map(
                ord,
                '%04d' % (i, )
            ))
            tmp.extend(buf[begin:end])
            yield tmp
            i = i + 1
            begin = end
            end = end + chunk_size
        tmp = bytearray(map(
            ord,
            '%04d' % (i, )
        ))
        tmp.extend(buf[begin:end])
        yield tmp

    def pack_header(self, s):
        # get the file from the message
        # find file and fill the header
        file_name = sys.stdin.readline()
        file_name = file_name[:220]
        if not os.path.isfile(file_name):
            sys.stdout.write('[Me] %s does not exists\n' % file_name)
            sys.stdout.write('[Me] ')
            sys.stdout.flush()
        else:
            file_size = os.stat(file_name).st_size

            packet_size = 256
            file_name_size = len(file_name)
            file_type = 1
            chunk_size = 1000
            num_of_chunks = file_size / chunk_size + file_size % chunk_size
            # create the header

            # HEADER
            # size >> data

            # 004  >>  size of packet
            # 004  >>  file name size
            # 220  >>  file name
            # 016  >>  file size
            # 004  >>  type
            # 004  >>  size of chunks
            # 004  >>  number of blocks

            header = bytearray()
            # size of packet
            header.extend(bytearray(map(
                ord,
                '%04d' % (packet_size, )
            )))
            # file name size
            header.extend(bytearray(map(
                ord,
                '%04d' % (file_name_size, )
            )))
            # file name
            header.extend(bytearray(map(
                ord,
                file_name
            )))
            header.extend(bytearray(220 - file_name_size))
            # file size
            header.extend(bytearray(map(
                ord,
                '%016d' % (file_size, )
            )))
            # type
            header.extend(bytearray(map(
                ord,
                '%04d' % (file_type, )
            )))
            # chunk size
            header.extend(bytearray(map(
                ord,
                '%04d' % (chunk_size, )
            )))
            # num of blocks
            header.extend(bytearray(map(
                ord,
                '%04d' % (num_of_chunks, )
            )))
            sys.stdout.write('[Me] %d\n' % len(header))
            s.send(header)
            sys.stdout.write('[Me] ')
            sys.stdout.flush()

    def unpack_header(self, arr):
        result = []
        sys.stdout.write('[Me] ')
        # 004  >>  size of packet
        packet_size = int(''.join(map(chr, arr[:4])))
        result.append(packet_size)
        sys.stdout.write("packet_size %d\n" % packet_size)
        # 004  >>  file name size
        file_name_size = int(''.join(map(chr, arr[4:8])))
        result.append(file_name_size)
        sys.stdout.write("file_name_size %d\n" % file_name_size)
        # 220  >>  file name
        file_name = ''.join(map(chr, arr[8: 8 + file_name_size]))
        result.append(file_name)
        sys.stdout.write("file_name %s\n" % file_name)
        # 016  >>  file size
        file_size = int(''.join(map(chr, arr[228:244])))
        result.append(file_size)
        sys.stdout.write("file_size %d\n" % file_size)
        # 004  >>  type
        file_type = ''.join(map(chr, arr[244:248]))
        result.append(file_type)
        sys.stdout.write("file_type %s\n" % file_type)
        # 004  >>  size of chunks
        chunk_size = int(''.join(map(chr, arr[248:252])))
        result.append(chunk_size)
        sys.stdout.write("chunk_size %d\n" % chunk_size)
        # 004  >>  number of blocks
        num_of_chunks = int(''.join(map(chr, arr[252:])))
        result.append(num_of_chunks)
        sys.stdout.write("num_of_chunks %d\n" % num_of_chunks)
        sys.stdout.flush()
        return result

    def unpack_chunk(self, arr):
        result = []
        sys.stdout.write('[Me] ')
        # 0004  >> chunk number
        chunk_number = int(''.join(map(chr, arr[:4])))
        result.append(chunk_number)
        sys.stdout.write("chunk_number %d\n" % chunk_number)
        # 1000  >>  file buffer
        file_chunk = arr[4:1004]
        result.append(file_chunk)
        sys.stdout.write("file_chunk %d\n" % file_chunk)
        sys.stdout.flush()
        return result

    def get_all_chunks(self, chunks):
        result = bytearray()
        for chunk in chunks:
            result.extend(self, self.unpack_chunk(chunk)[1])
        return result

    def write_file(file_name, barray):
        result = open(file_name, "wb")
        result.write(barray)

    def save_chunks(self, file_name, chunks):
        self.write_file(file_name, self.get_all_chunks(chunks))

    def run(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.settimeout(2)

        # connect to remote host
        try:
            s.connect((self.HOST, self.PORT))
        except:
            print 'Unable to connect'
            sys.exit()

        print 'Connected to remote host. You can start sending messages'
        sys.stdout.write('[Me] ')
        sys.stdout.flush()

        while 1:
            socket_list = [sys.stdin, s]

            # Get the list sockets which are readable
            read_sockets, write_sockets, error_sockets = select.select(
                socket_list,
                [],
                []
            )

            for sock in read_sockets:
                if sock == s:
                    # incoming message from remote server, s
                    data = sock.recv(1024)
                    header = bytearray(data)
                    if not header:
                        print '\nDisconnected from chat server'
                        sys.exit()
                    elif data[0] == "[":
                        sys.stdout.write('[Me] %s\n' % data)
                    else:
                        self.unpack_header(header)

                else:
                    self.pack_header(s)


if __name__ == "__main__":
    if(len(sys.argv) < 3):
        print 'Usage : python chat_client.py hostname port'
        sys.exit()

    host = sys.argv[1]
    port = int(sys.argv[2])

    client = Client(host, port)

    sys.exit(client.run())
