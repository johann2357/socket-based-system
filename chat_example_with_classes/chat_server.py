import sys
import socket
import select
import os
import subprocess
import time


class Chunks(object):

    def read_into_buffer(self, filename):
        buf = bytearray(os.path.getsize(filename))
        with open(filename, 'rb') as f:
            f.readinto(buf)
        return buf

    def chunk_generator(self, file_name, chunk_size):
        buf = self.read_into_buffer(file_name)
        size = os.path.getsize(file_name)
        begin, end = 0, chunk_size
        i = 0
        while end < size:
            tmp = bytearray(map(
                ord,
                '%04d' % (i, )
            ))
            tmp.extend(buf[begin:end])
            yield tmp
            i = i + 1
            begin = end
            end = end + chunk_size
        tmp = bytearray(map(
            ord,
            '%04d' % (i, )
        ))
        tmp.extend(buf[begin:end])
        yield tmp

    def pack_header(self, s, file_name):
        # get the file from the message
        # find file and fill the header
        # file_name = sys.stdin.readline()
        file_name = file_name[:220]
        print "file_name: ", file_name
        if not os.path.isfile(file_name):
            sys.stdout.write('[Me] %s does not exists\n' % file_name)
            # sys.stdout.write('[Me] ')
            sys.stdout.flush()
        else:
            file_size = os.stat(file_name).st_size

            packet_size = 256
            file_name_size = len(file_name)
            file_type = 1
            chunk_size = 1000
            num_of_chunks = file_size / chunk_size + file_size % chunk_size
            # create the header

            # HEADER
            # size >> data

            # 004  >>  size of packet
            # 004  >>  file name size
            # 220  >>  file name
            # 016  >>  file size
            # 004  >>  type
            # 004  >>  size of chunks
            # 004  >>  number of blocks

            header = bytearray()
            # size of packet
            header.extend(bytearray(map(
                ord,
                '%04d' % (packet_size, )
            )))
            # file name size
            header.extend(bytearray(map(
                ord,
                '%04d' % (file_name_size, )
            )))
            # file name
            header.extend(bytearray(map(
                ord,
                file_name
            )))
            header.extend(bytearray(220 - file_name_size))
            # file size
            header.extend(bytearray(map(
                ord,
                '%016d' % (file_size, )
            )))
            # type
            header.extend(bytearray(map(
                ord,
                '%04d' % (file_type, )
            )))
            # chunk size
            header.extend(bytearray(map(
                ord,
                '%04d' % (chunk_size, )
            )))
            # num of blocks
            header.extend(bytearray(map(
                ord,
                '%04d' % (num_of_chunks, )
            )))
            sys.stdout.write('[Me] %d\n' % len(header))
            s.send(header)
            sys.stdout.write('[Me] ')
            sys.stdout.flush()

    def unpack_header(self, arr):
        result = []
        sys.stdout.write('[Me] ')
        # 004  >>  size of packet
        packet_size = int(''.join(map(chr, arr[:4])))
        result.append(packet_size)
        sys.stdout.write("packet_size %d\n" % packet_size)
        # 004  >>  file name size
        file_name_size = int(''.join(map(chr, arr[4:8])))
        result.append(file_name_size)
        sys.stdout.write("file_name_size %d\n" % file_name_size)
        # 220  >>  file name
        file_name = ''.join(map(chr, arr[8: 8 + file_name_size]))
        result.append(file_name)
        sys.stdout.write("file_name %s\n" % file_name)
        # 016  >>  file size
        file_size = int(''.join(map(chr, arr[228:244])))
        result.append(file_size)
        sys.stdout.write("file_size %d\n" % file_size)
        # 004  >>  type
        file_type = ''.join(map(chr, arr[244:248]))
        result.append(file_type)
        sys.stdout.write("file_type %s\n" % file_type)
        # 004  >>  size of chunks
        chunk_size = int(''.join(map(chr, arr[248:252])))
        result.append(chunk_size)
        sys.stdout.write("chunk_size %d\n" % chunk_size)
        # 004  >>  number of blocks
        num_of_chunks = int(''.join(map(chr, arr[252:])))
        result.append(num_of_chunks)
        sys.stdout.write("num_of_chunks %d\n" % num_of_chunks)
        sys.stdout.flush()
        return result

    def unpack_chunk(self, arr):
        result = []
        sys.stdout.write('[Me] ')
        # 0004  >> chunk number
        chunk_number = int(''.join(map(chr, arr[:4])))
        result.append(chunk_number)
        sys.stdout.write("chunk_number %d\n" % chunk_number)
        # 1000  >>  file buffer
        file_chunk = arr[4:1004]
        result.append(file_chunk)
        sys.stdout.write("file_chunk %d\n" % file_chunk)
        sys.stdout.flush()
        return result

    def get_all_chunks(self, chunks):
        result = bytearray()
        for chunk in chunks:
            result.extend(self, self.unpack_chunk(chunk)[1])
        return result

    def write_file(file_name, barray):
        result = open(file_name, "wb")
        result.write(barray)

    def save_chunks(self, file_name, chunks):
        self.write_file(file_name, self.get_all_chunks(chunks))


class Server(object):
    HOST = ''
    SOCKET_LIST = []
    RECV_BUFFER = 4096
    PORT = 9009
    num_available_execs = 0
    states = []
    chunks = Chunks()

    def update_states(self):
        print "updating states"
        for i, state in enumerate(self.SOCKET_LIST):
            self.states.append({'slave': -1, 'state': "available"})

    def get_execs(self, path):
        # print path
        # os.chdir(path)
        os.system("ls")
        output = subprocess.check_output("find . -type f | wc -l", shell=True)
        self.num_available_execs = int(output) - 7
        for i, state in enumerate(self.SOCKET_LIST):
            self.states.append({'slave': -1, 'state': "available"})
        # print "self.states: ", self.states

    def run(self):
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        server_socket.bind((self.HOST, self.PORT))
        server_socket.listen(10)

        # add server socket object to the list of readable connections
        self.SOCKET_LIST.append(server_socket)

        print "Chat server started on port " + str(self.PORT)

        while 1:

            # get the list sockets which are ready to be read through select
            # 4th arg, time_out  = 0 : poll and never block
            ready_to_read, ready_to_write, in_error = select.select(
                self.SOCKET_LIST,
                [],
                [],
                0
            )
            for sock in ready_to_read:
                # a new connection request recieved
                if sock == server_socket:
                    sockfd, addr = server_socket.accept()
                    self.SOCKET_LIST.append(sockfd)
                    self.update_states()
                    print "insert start to execute exes in slaves"
                    msg = sys.stdin.readline()
                    # print "msg: #" +  msg + "#"
                    if msg == "start\n":
                        print "msg inside: ", msg
                        self.broadcast_data_to_slaves()
                    # print "Client (%s, %s) connected" % addr

                    # self.broadcast(
                    #     server_socket,
                    #     sockfd,
                    #     "[%s:%s] entered our chatting room\n" % addr
                    # )

                # a message from a client, not a new connection
                else:
                    # process data recieved from client,
                    try:
                        # receiving data from the socket.
                        data = sock.recv(self.RECV_BUFFER)
                        if data:
                            # there is something in the socket
                            # self.broadcast(
                            #     server_socket,
                            #     sock,
                            #     "\r" + '[' + str(sock.getpeername()) + '] ' + data
                            # )
                            text_file = open(sock+"_client.txt", "w")
                            text_file.write(data)
                            text_file.close()
                        else:
                            # remove the socket that's broken
                            if sock in self.SOCKET_LIST:
                                self.SOCKET_LIST.remove(sock)

                            # at this stage, no data means probably the
                            # connection has been broken
                            self.broadcast(
                                server_socket,
                                sock,
                                "Client (%s, %s) is offline\n" % addr
                            )

                    # exception
                    except:
                        self.broadcast(
                            server_socket,
                            sock,
                            "Client (%s, %s) is offline\n" % addr
                        )
                        continue

        server_socket.close()

    def send_data_to_slave(self, file_pos, socket_pos):
        print "file_pos: ", file_pos
        print "socket_pos: ", socket_pos + 1
        self.chunks.pack_header(
            self.SOCKET_LIST[socket_pos+1],
            "exe_"+str(file_pos)+".out")

        for chunk in self.chunks.chunk_generator("exe_"+str(file_pos)+".out", 1000):
            self.SOCKET_LIST[socket_pos].send(chunk)

    def broadcast_data_to_slaves(self):
        print "self.SOCKET_LIST: ", self.SOCKET_LIST
        list_execs = range(self.num_available_execs)
        while list_execs:
            num_exe = list_execs.pop()

            pos = 0
            while True:
                num_slaves = len(self.SOCKET_LIST)
                print "num_slaves: ", num_slaves-1
                if pos >= num_slaves:
                    pos = pos % num_slaves
                print "pos: ", pos
                print "state.get('state'): ", self.states[pos].get('state')
                time.sleep(2)
                if self.states[pos].get('state') == 'available':
                    print 'pos: ', pos
                    # position of slave socket in state
                    self.states[pos]['slave'] = pos
                    self.states[pos]['state'] = 'no_available'
                    self.send_data_to_slave(num_exe, pos)
                    break
                pos += 1

    # broadcast chat messages to all connected clients
    def broadcast(self, server_socket, sock):
        # for sockett in self.SOCKET_LIST:
        #     # send the message only to peer
        #     self.broadcast_data_to_slaves()
        #     if sockett != server_socket and sockett != sock:
        #         try:
        #             sockett.send(message)
        #         except:
        #             # broken socket connection
        #             sockett.close()
        #             # broken socket, remove it
        #             if sockett in self.SOCKET_LIST:
        #                 self.SOCKET_LIST.remove(sockett)

        self.broadcast_data_to_slaves()


if __name__ == "__main__":
    server = Server()
    server.get_execs("")
    sys.exit(server.run())
